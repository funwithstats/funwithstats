const bettingtds = document.getElementById("betBoard").getElementsByTagName("td");

const numbers = [
	"00", 27, 10, 25, 29, 12, 8, 19, 31, 18, 6, 21, 33, 16, 4, 23, 35, 14, 2, "0", 28, 9, 26, 30, 11, 7, 20, 32, 17, 5, 22, 34, 15, 3, 24, 36, 13, 1
];

const cashEl = document.getElementById("cash");
const betEl = document.getElementById("bet");
const betOnEl = document.getElementById("beton");
const chancesEl = document.getElementById("chances");
const payoutEl = document.getElementById("payout");
const errorEl = document.getElementById("error");

const resultDiv = document.getElementById("resultWrapper");
const resultEl = document.getElementById("result");
const winningsEl = document.getElementById("winnings");
const ballEl = document.getElementById("ball");

let payout = {};
let chances = {};

payout["1st 12"] = "2 to 1";
chances["1st 12"] = "6 to 13";

payout["2nd 12"] = "2 to 1";
chances["2nd 12"] = "6 to 13";

payout["3rd 12"] = "2 to 1";
chances["3rd 12"] = "6 to 13";

payout["red"] = "1 to 1";
chances["red"] = "9 to 10";

payout["black"] = "1 to 1";
chances["black"] = "9 to 10";

payout["Even"] = "1 to 1";
chances["Even"] = "9 to 10";

payout["Odd"] = "1 to 1";
chances["Odd"] = "9 to 10";

payout["1 to 18"] = "1 to 1";
chances["1 to 18"] = "9 to 10";

payout["19 to 36"] = "1 to 1";
chances["19 to 36"] = "9 to 10";

payout["single number"] = "35 to 1";
chances["single number"] = "1 to 37";

let wheel = [];

let color = "redWheel";

for( let num of numbers ){
	
	wheel[num]= color;
	
	if(color == "redWheel"){
		color = "blackWheel"
	} else{
		color = "redWheel";
	}
	
	if(typeof num == "string"){
		wheel[num] = "greenWheel";
	}
	
}

for( let td of bettingtds ){
	td.onclick = function() {
		let currentSelection = document.getElementsByClassName("selected");
		
		for( let el of currentSelection ){
			el.classList.remove("selected");
		}
		
		this.classList.add("selected");
		
		let newBet = this.textContent;
		
		if(document.querySelector(".red.selected")){
			newBet = "red";
		}
		
		if(document.querySelector(".black.selected")){
			newBet = "black";
		}
		
		betOnEl.textContent = newBet;
		
		if( !isNaN(newBet) ){
			newBet = "single number";
		}
		
		chancesEl.textContent = chances[newBet];
		payoutEl.textContent = payout[newBet];
		
	}
	
	td.classList.add(wheel[td.textContent]);
	
}

function win(bet){
	
	if( !isNaN(bet) ){
		bet = "single number";
	}
	
	let winFactor = payout[bet].match(/^\d+/g)[0];
	
	let currCash = cash.textContent * 1;
	
	let winnings = betEl.value * winFactor;
	
	currCash += winnings;
	
	cashEl.textContent = Math.round(currCash * 100)/100;
	resultEl.textContent = "won";
	winningsEl.textContent = winnings;
}

function lose(){
	let currCash = cashEl.textContent;
	let winnings = betEl.value;
	currCash -= winnings;
	cashEl.textContent = Math.round(currCash * 100)/100;
	resultEl.textContent = "lost";
	winningsEl.textContent = winnings;
	
	if(currCash <= 0){
		
		setTimeout(function(){
		
			let response = "xxx";
			
			while( isNaN(response) ){
				response = prompt("You have run out of money! Enter the amount of money you would like to start with, and try again!", 1000);
				
				if(!response){
					return;
				}
				
			}
			
			cashEl.textContent = response;
			
		}, 100);
		
	}
	
}

function playRoulette(test = null){
	
	let currentCash = cashEl.textContent;
	
	if(currentCash <= 0){
		let response = "xxx";
		
		while( isNaN(response) ){
			response = prompt("You have run out of money! Enter the amount of money you would like to start with, and try again!", 1000);
			
			if(!response){
				return;
			}
			
		}
		
		cashEl.textContent = response;
		return;
	}
	
	
	let betAmnt = betEl.value * 1;
	let currCash = cashEl.textContent * 1;
	
	if( betAmnt > currCash ){
		errorEl.textContent = "You do not have enough cash to place your bet.";
		resultDiv.classList.add("hide");
		return;
	} else if( betAmnt < 0 ){
		errorEl.textContent = "You cannot place a negative bet.";
		resultDiv.classList.add("hide");
		return;
	}
	
	errorEl.textContent = "";
	
	resultDiv.classList.remove("hide");
	
	let bet = document.getElementsByClassName("selected")[0].textContent;
	
	if(document.querySelector(".red.selected")){
		bet = "red";
	}
	
	if(document.querySelector(".black.selected")){
		bet = "black";
	}
	
	let ball = randomInt(0, 37);
	
	if(test != null){
		ball = test;
	}
	
	ballEl.textContent = ball;
	
	if( ball == bet || ( ball == 37 && bet == "00" ) ){
		win(bet);
		return;
	} else if( ball == 37 || ball == 0){
		lose();
		return;
	}
	
	if( bet == "Even" && ball % 2 == 0 ){
		win(bet);
		return;
	}
	
	if( bet == "Odd" && ball % 2 != 0 ){
		win(bet);
		return;
	}
	
	if( bet == "19 to 36" && ball >= 19 ){
		win(bet);
		return;
	}
	
	if( bet == "1 to 18" && ball <= 18 ){
		win(bet);
		return;
	}
	
	if( bet == "1st 12" && ball <= 12 ){
		win(bet);
		return;
	}
	
	if( bet == "2nd 12" && ball <= 24 && ball >= 13 ){
		win(bet);
		return;
	}
	
	if( bet == "3rd 12" && ball >= 25 ){
		win(bet);
		return;
	}
	
	if( bet == "red" && wheel[ball] == "redWheel" ){
		win(bet);
		return;
	}
	
	if( bet == "black" && wheel[ball] == "blackWheel" ){
		win(bet);
		return;
	}
	
	lose();
	
}