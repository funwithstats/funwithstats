const table = document.getElementById("chidata").children[0];
const totalEl = document.getElementById("totalChi");
const freeEl = document.getElementById("degFree");
const pChiEl = document.getElementById("pChi");
const canvas = document.getElementById("chiGraph");
const resultsEl = document.getElementById("dipslayResults");

var context = canvas.getContext("2d");

let myGraph = setUpGraph(canvas, context, -3, 3, -0.5, 0.5, 500, 500);

let chiStat = 0;
let degFree = 0;

var timer = 0;

function onUpdate(tr){
	
	if(timer){
		clearTimeout(timer);
	}
	
	timer = setTimeout(function(){updateRow(tr);}, 1000);
}

function updateRow(tr){
	
	if(timer){
		clearTimeout(timer);
	}
	
	let obsEl = tr.getElementsByClassName("observed")[0];
	let expEl = tr.getElementsByClassName("expected")[0];
	let chiEl = tr.getElementsByClassName("chi")[0];
	
	let obs = obsEl.value;
	let exp = expEl.value;
	
	if( obs !== "" && exp !== "" ){
		chiEl.textContent = (( (obs - exp) ** 2 ) / exp).toFixed(2);
	} else {
		chiEl.textContent = "";
	}
	
	let chiEls = document.getElementsByClassName("chi");
	
	let sum = 0;
	let degreesOfFreedom = -1;
	
	for(let td of chiEls){
		if(td.innerText.length > 0){
			sum += parseFloat(td.innerText);
			degreesOfFreedom++;
		}
	}
	
	if(degreesOfFreedom > 0 && ( sum != chiStat || degFree != degreesOfFreedom ) ){
		resultsEl.classList.remove("hide");
		chiStat = sum;
		degFree = degreesOfFreedom;
		myGraph = reboundGraph(myGraph, -1, 3 + 3 * degreesOfFreedom, Math.min(-.25 / degreesOfFreedom, -.025), Math.max(1 / degreesOfFreedom, .1))
		myGraph.shadeUnderCurve(chiSquarePDFFunc(degreesOfFreedom), Math.min(chiStat, myGraph.xMax), myGraph.xMax + 2);
		
		totalEl.textContent = chiStat;
		freeEl.textContent = degFree;
	} else if(degreesOfFreedom <= 0){
		resultsEl.classList.add("hide");
	}
	
	pChiEl.textContent = ((1 - computeChi(sum, degreesOfFreedom)) * 100).toFixed(2)
	
}

function addRow(){
	let rows = table.getElementsByTagName("tr");
	let lastRow = rows[rows.length - 1];
	
	let newRow = lastRow.cloneNode(true);
	
	for(let inp of newRow.getElementsByTagName("input")){
		inp.value = "";
	}
	
	newRow.getElementsByClassName("chi")[0].textContent = "";
	
	table.appendChild(newRow);
}

function removeRow(){
	let rows = table.getElementsByTagName("tr");
	
	let lastRow = rows[rows.length - 1];
	
	if(rows.length < 3){
		alert("You cannot remove the only row of data.");
		return;
	} else if(
			(lastRow.getElementsByClassName("observed")[0].value.length < 1
			&& lastRow.getElementsByClassName("expected")[0].value.length < 1
			&& lastRow.getElementsByClassName("groupName")[0].value.length < 1
			)
			|| confirm("Would you like to remove the last row of data?")
		)
			{
		
		lastRow.remove();
		updateRow(rows[1]);
	}
	
}