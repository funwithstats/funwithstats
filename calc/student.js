function LogGamma(Z) {
	with (Math) {
		var S=1+76.18009173/Z-86.50532033/(Z+1)+24.01409822/(Z+2)-1.231739516/(Z+3)+.00120858003/(Z+4)-.00000536382/(Z+5);
		var LG= (Z-.5)*log(Z+4.5)-(Z+4.5)+log(S*2.50662827465);
	}
	return LG;
}

function gammaConvert(x){
	return Math.exp(LogGamma(x));
}

function Betinc(X,A,B) {
	var A0=0;
	var B0=1;
	var A1=1;
	var B1=1;
	var M9=0;
	var A2=0;
	var C9;
	while (Math.abs((A1-A2)/A1)>.00001) {
		A2=A1;
		C9=-(A+M9)*(A+B+M9)*X/(A+2*M9)/(A+2*M9+1);
		A0=A1+C9*A0;
		B0=B1+C9*B0;
		M9=M9+1;
		C9=M9*(B-M9)*X/(A+2*M9-1)/(A+2*M9);
		A1=A0+C9*A1;
		B1=B0+C9*B1;
		A0=A0/B1;
		B0=B0/B1;
		A1=A1/B1;
		B1=1;
	}
	return A1/A;
}

function findt(percent, degFree, iter = 25){
	let t = 0;
	let delta = 3;
	
	let changeDelta = false;
	
	
	let testPercent = tCDF(t, degFree);
	
	for(let i = 0; i < iter; i++){
		
		if(testPercent == percent){
			return t;
		}
		
		if(testPercent * 100 < percent){
			t += delta;
		} else {
			t -= delta;
		}
		
		testPercent = tCDF(t, degFree);
		
		if(!changeDelta){
			console.log(t);
			console.log(testPercent * 100);
			console.log(percent);
			if(t < 0 && testPercent * 100 < percent){
				changeDelta = true;
			}else if(t > 0 && testPercent * 100 > percent){
				changeDelta = true;
			}
			
		}
		
		if(changeDelta){
			delta /= 2;
		}
		
	}
	console.log(changeDelta);
	return t;
}

function tCDF(xVal, DegFree) {
    X=eval(xVal);
    df=eval(DegFree);
    with (Math) {
		if (df<=0) {
			//alert("Degrees of freedom must be positive")
		} else {
			A=df/2;
			S=A+.5;
			Z=df/(df+X*X);
			BT=exp(LogGamma(S)-LogGamma(.5)-LogGamma(A)+A*log(Z)+.5*log(1-Z));
			if (Z<(A+1)/(S+2)) {
				betacdf=BT*Betinc(Z,A,.5);
			} else {
				betacdf=1-BT*Betinc(1-Z,.5,A);
			}
			if (X<0) {
				tcdf=betacdf/2;
			} else {
				tcdf=1-betacdf/2;
			}
		}
		tcdf=round(tcdf*100000)/100000;
	}
    return tcdf;
}

function tPDF(xVal, DegFree){
	let pdf = "( ( gammaConvert((degFree+1)/2) ) / ( Math.sqrt(Math.PI * degFree) * gammaConvert(degFree/2) ) ) * ( 1 + ( ( (x) ** 2) / degFree ) ) ** ( -1 * ( degFree + 1 ) / 2 )";
	
	pdf = pdf.replaceAll("x", xVal).replaceAll("degFree", DegFree)
	
	return eval(pdf);
}

function tPDFFunc(degFree){
	return `tPDF(x, ${degFree})`;
}