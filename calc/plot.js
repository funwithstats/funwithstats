const tbody = document.getElementById("plotdata").children[0];
const canvas = document.getElementById("plot");
const residCanvas = document.getElementById("residualPlot");
const resultsEl = document.getElementById("dipslayResults");
const dataCont = document.getElementById("datacontainer");

const xMinInp = document.getElementById("xMin");
const xMaxInp = document.getElementById("xMax");
const yMinInp = document.getElementById("yMin");
const yMaxInp = document.getElementById("yMax");

const xMinResidInp = document.getElementById("xMinResid");
const xMaxResidInp = document.getElementById("xMaxResid");
const yMinResidInp = document.getElementById("yMinResid");
const yMaxResidInp = document.getElementById("yMaxResid");
const boundErrorResid = document.getElementById("boundaryErrorResid");

const showRegressionCheck = document.getElementById("showRegression");
const plotrDisp = document.getElementById("plotr");
const sdxDisp = document.getElementById("sdx");
const sdyDisp = document.getElementById("sdy");
const RMSErrorDisp = document.getElementById("RMSError");
const avexDisp = document.getElementById("avex");
const aveyDisp = document.getElementById("avey");
const regressionDisp = document.getElementById("regression");
const staticTbody = document.getElementById("staticData").children[0];
const boundError = document.getElementById("boundaryError");

let context = canvas.getContext("2d");
let residContext = residCanvas.getContext("2d");

let myGraph = setUpGraph(canvas, context, -3, 3, -0.5, 0.5, 500, 500);

let residPlot = setUpGraph(residCanvas, residContext, -3, 3, -0.5, 0.5, 500, 500);

function addRow(){
	let rows = tbody.getElementsByTagName("tr");
	let lastRow = rows[rows.length - 1];
	
	let newRow = lastRow.cloneNode(true);
	
	for(let inp of newRow.getElementsByTagName("input")){
		inp.value = "";
	}
	
	tbody.appendChild(newRow);
}

function removeRow(){
	let rows = tbody.getElementsByTagName("tr");
	
	let lastRow = rows[rows.length - 1];
	
	if(rows.length < 3){
		alert("You cannot remove the only row of data.");
		return;
	} else if(
			(lastRow.getElementsByClassName("yval")[0].value.length < 1
			&& lastRow.getElementsByClassName("xval")[0].value.length < 1
			)
			|| confirm("Would you like to remove the last row of data?")
		)
			{
		
		lastRow.remove();
	}
	
}

function plot(autobound = true){
	
	let yinps = document.getElementsByClassName("yval");
	let xinps = document.getElementsByClassName("xval");
	
	let x = [];
	let y = [];
	
	let xMax = false;
	let xMin = false;
	let yMax = false;
	let yMin = false;
	
	for(let xinp of xinps){
		
		let parsedX = parseFloat(xinp.value);
		
		if(isNaN(parsedX)){
			alert("You must put a number in each cell.");
			return;
		}
		
		if( xMax === false){
			// first piece of data
			xMax = parsedX;
			xMin = parsedX;
		}
		
		x.push(parsedX);
		
		if(parsedX > xMax){
			xMax = parsedX;
		}
		
		if(parsedX < xMin){
			xMin = parsedX;
		}
		
	}
	
	for(let yinp of yinps){
		
		let parsedY = parseFloat(yinp.value);
		
		if(isNaN(parsedY)){
			alert("You must put a number in each cell.");
			return;
		}
		
		if( yMax === false){
			// first piece of data
			yMax = parsedY;
			yMin = parsedY;
		}
		
		y.push(parsedY);
		
		if(parsedY > yMax){
			yMax = parsedY;
		}
		
		if(parsedY < yMin){
			yMin = parsedY;
		}
		
	}
	
	let sdX = sd(x);
	let sdY = sd(y);
	
	sdxDisp.textContent = sdX.toFixed(2);
	sdyDisp.textContent = sdY.toFixed(2);
	
	let avex = average(x);
	let avey = average(y);
	
	avexDisp.textContent = avex.toFixed(2);
	aveyDisp.textContent = avey.toFixed(2);
	
	let r = getr(x, y);
	
	if(sdX == 0 || sdY == 0){
		RMSErrorDisp.textContent = "N/A";
		plotrDisp.textContent = "N/A";
	} else {
		RMSErrorDisp.textContent = (Math.sqrt(1 - (r ** 2)) * sdY).toFixed(4);
		plotrDisp.textContent = r.toFixed(2);
	}
	
	let regressionObj = getRegression(x, y);
	
	let regDisp = regressionObj.display;
	let regActual = regressionObj.actual;
	
	regressionDisp.textContent = regDisp;
	
	
	
	if(autobound){
		let extraX = .1 * Math.max(xMax - xMin, 5);
		let extraY = .1 * Math.max(yMax - yMin, 5);
		let plotXMin = (xMin - extraX).toFixed(2)*1;
		let plotXMax = (xMax + extraX).toFixed(2)*1;
		let plotYMin = (yMin - extraY).toFixed(2)*1;
		let plotYMax = (yMax + extraY).toFixed(2)*1;
		
		xMinInp.value = plotXMin.toFixed(2);
		xMaxInp.value = plotXMax.toFixed(2);
		yMinInp.value = plotYMin.toFixed(2);
		yMaxInp.value = plotYMax.toFixed(2);
		
		myGraph = reboundGraph(myGraph, plotXMin, plotXMax, plotYMin, plotYMax);
	}
	
	if(showRegressionCheck.checked){
		myGraph.drawGraph(regActual);
	} else {
		myGraph.clearGraph();
	}
	
	residPlot.clearGraph();
	
	for( let i in x ){
		myGraph.drawPoint(x[i], y[i]);
	}
	
	staticTbody.innerHTML = "";
	
	let residuals = [];
	let predictions = [];
	
	for( let tr of tbody.children ){
		
		let newTr = tr.cloneNode(true);
		
		let rowData = [];
		
		for(let td of newTr.children){
			if(td.children.length > 0){
				// the td has an input. Get its value and make it static.
				td.innerHTML = td.children[0].value;
				rowData.push(td.innerHTML * 1);
			}
		}
		
		if(rowData.length < 2){
			let lastHeader = `<th class="predictionTD">Predictions</th><th class="residualTD">Residuals</th>`;
			newTr.innerHTML += lastHeader;
		} else {
			let expected = eval(regActual.replaceAll("x", rowData[0]));
			let residual = rowData[1] - expected;
			
			residuals.push(residual);
			predictions.push(expected);
			
			if( Math.abs(residual) !== Infinity ){
				let predictionTD = `<td class="predictionTD">${expected.toFixed(4)}</td>`;
				let residualTD = `<td class="residualTD">${residual.toFixed(4)}</td>`;
				newTr.innerHTML += predictionTD + residualTD;
			}
		}
		
		staticTbody.appendChild(newTr);
		
	}
	
	if(autobound){
		let maxResid = Math.max.apply(this, residuals);
		let minResid = Math.min.apply(this, residuals);
		
		let extraY = .1 * Math.max(5, maxResid - minResid);
		
		let residPlotXMin = xMinInp.value;
		let residPlotXMax = xMaxInp.value;
		
		let residPlotYMin = (minResid - extraY).toFixed(2) * 1;
		let residPlotYMax = (maxResid + extraY).toFixed(2) * 1;
		
		xMinResidInp.value = residPlotXMin;
		xMaxResidInp.value = residPlotXMax;
		
		yMinResidInp.value = residPlotYMin;
		yMaxResidInp.value = residPlotYMax;
		
		residPlot = reboundGraph(residPlot, residPlotXMin, residPlotXMax, residPlotYMin, residPlotYMax);
		
	}
	
	for( let i in x ){
		residPlot.drawPoint(x[i], residuals[i]);
	}
	
	dataCont.classList.add("hide");
	resultsEl.classList.remove("hide");
}

function editData(){
	resultsEl.classList.add("hide");
	dataCont.classList.remove("hide");
	myGraph.clearGraph();
	residPlot.clearGraph();
}

let timer = null;

function reboundPlot(){
	
	if(timer){
		clearTimeout(timer);
	}
	
	if( isNaN(parseFloat(xMinInp.value)) || isNaN(parseFloat(xMaxInp.value)) || isNaN(parseFloat(yMinInp.value)) || isNaN(parseFloat(yMaxInp.value)) ){
		// don't try to rebound the graph if we are missing one or more values.
		boundError.textContent = "ERROR: One or more of your boundaries is not a valid number.";
		return;
	}
	
	if(xMinInp.value > xMaxInp.value){
		// boundaries are flipped in the x direction. Let the user know...
		boundError.textContent = "ERROR: Your upper x bound is less than your lower x bound.";
		return;
	}
	
	if(yMinInp.value > yMaxInp.value){
		// boundaries are flipped in the y direction. Let the user know...
		boundError.textContent = "ERROR: Your upper y bound is less than your lower y bound.";
		return;
	}
	
	boundError.textContent = "";
	
	timer = setTimeout( function(){
		let plotXMin = xMinInp.value;
		let plotXMax = xMaxInp.value;
		let plotYMin = yMinInp.value;
		let plotYMax = yMaxInp.value;
		
		myGraph = reboundGraph(myGraph, plotXMin, plotXMax, plotYMin, plotYMax);
		
		let autobound = false;
		
		plot(autobound);
	}, 500);
	
}

let timer2 = null;

function reboundPlotResid(){
	
	if(timer2){
		clearTimeout(timer2);
	}
	
	if( isNaN(parseFloat(xMinResidInp.value)) || isNaN(parseFloat(xMaxResidInp.value)) || isNaN(parseFloat(yMinResidInp.value)) || isNaN(parseFloat(yMaxResidInp.value)) ){
		// don't try to rebound the graph if we are missing one or more values.
		boundErrorResid.textContent = "ERROR: One or more of your boundaries is not a valid number.";
		return;
	}
	
	if(xMinResidInp.value > xMaxResidInp.value){
		// boundaries are flipped in the x direction. Let the user know...
		boundError.textContent = "ERROR: Your upper x bound is less than your lower x bound.";
		return;
	}
	
	if(yMinResidInp.value > yMaxResidInp.value){
		// boundaries are flipped in the y direction. Let the user know...
		boundErrorResid.textContent = "ERROR: Your upper y bound is less than your lower y bound.";
		return;
	}
	
	boundErrorResid.textContent = "";
	
	timer2 = setTimeout( function(){
		let plotXMin = xMinResidInp.value;
		let plotXMax = xMaxResidInp.value;
		let plotYMin = yMinResidInp.value;
		let plotYMax = yMaxResidInp.value;
		
		residPlot = reboundGraph(residPlot, plotXMin, plotXMax, plotYMin, plotYMax);
		
		let autobound = false;
		
		plot(autobound);
	}, 500);
	
}